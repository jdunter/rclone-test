import configparser
import os
import shutil
import time
from pathlib import Path
from subprocess import PIPE, Popen

# from src.rClone import create_folder, delete_folder, create_file


def test_config_is_config_object(config):
    assert type(config) == configparser.ConfigParser


def test_get_value_from_config(config):
    assert config["DEFAULT"]["device1"] == "device1/"


def test_create_and_delete_folder():
    path = Path("example")
    folder_name = "folder"
    assert not os.path.exists(path / folder_name)

    os.makedirs(path / folder_name)
    assert os.path.exists(path / folder_name)

    shutil.rmtree(path)
    assert not os.path.exists(path / folder_name)
    assert not os.path.exists(path)


def test_setup(setup, config):
    assert os.path.exists(
        Path(config["DEFAULT"]["testpath"]) / config["DEFAULT"]["device1"]
    )


def test_create_file(setup):
    name = "testfile.txt"
    content = "lorem ipsum"
    path = setup["device1"]
    with open(path / name, "w") as f:
        f.write(content)
    assert os.path.exists(path / name)
    with open(path / name) as f:
        file_content = f.read()
        assert content == file_content


def test_delete_file(setup):
    name = "testfile.txt"
    content = "lorem ipsum"
    path = setup["device1"]
    with open(path / name, "w") as f:
        f.write(content)
    assert os.path.exists(path / name)
    os.remove(path / name)
    assert not os.path.exists(path / name)


def test_setup_files(setup_files):
    """
    GIVEN starting from zero
    WHEN setup_files is used
    THEN files should exist in local1, local2 and remote1
    """

    # assert device1
    assert os.path.isfile(setup_files["device1"] / "file1.txt")
    with open(setup_files["device1"] / "file1.txt") as f:
        assert f.read() == "content1"
    assert os.path.isfile(setup_files["device1"] / "file2.txt")
    with open(setup_files["device1"] / "file2.txt") as f:
        assert f.read() == "content2"
    assert os.path.isfile(setup_files["device1"] / "folder1" / "file3.txt")
    with open(setup_files["device1"] / "folder1" / "file3.txt") as f:
        assert f.read() == "content3"

    # assert device2
    assert os.path.isfile(setup_files["device2"] / "file1.txt")
    with open(setup_files["device2"] / "file1.txt") as f:
        assert f.read() == "content1"
    assert os.path.isfile(setup_files["device2"] / "file2.txt")
    with open(setup_files["device2"] / "file2.txt") as f:
        assert f.read() == "content2"
    assert os.path.isfile(setup_files["device2"] / "folder1" / "file3.txt")
    with open(setup_files["device2"] / "folder1" / "file3.txt") as f:
        assert f.read() == "content3"

    # assert remote1
    p = Popen(
        f"rclone ls {setup_files['remote1']} {setup_files['config_flag']}", stdout=PIPE
    )
    stdout, _ = p.communicate()
    assert "file1.txt" in stdout.decode()
    assert "file2.txt" in stdout.decode()
    assert "folder1/file3.txt" in stdout.decode()

    p = Popen(
        f"rclone cat {setup_files['remote1']}file1.txt {setup_files['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "content1" == stdout.decode()
    p = Popen(
        f"rclone cat {setup_files['remote1']}file2.txt {setup_files['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "content2" == stdout.decode()
    p = Popen(
        f"rclone cat {setup_files['remote1']}folder1/file3.txt {setup_files['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "content3" == stdout.decode()


def test_setup_bisync(setup_bisync):
    """
    GIVEN starting from zero
    WHEN setup_files is used
    THEN files should exist in local1, local2 and remote1
    """
    # assert device1
    assert os.path.isfile(setup_bisync["device1"] / "file1.txt")
    with open(setup_bisync["device1"] / "file1.txt") as f:
        assert f.read() == "content1"
    assert os.path.isfile(setup_bisync["device1"] / "file2.txt")
    with open(setup_bisync["device1"] / "file2.txt") as f:
        assert f.read() == "content2"
    assert os.path.isfile(setup_bisync["device1"] / "folder1" / "file3.txt")
    with open(setup_bisync["device1"] / "folder1" / "file3.txt") as f:
        assert f.read() == "content3"

    # assert device2
    assert os.path.isfile(setup_bisync["device2"] / "file1.txt")
    with open(setup_bisync["device2"] / "file1.txt") as f:
        assert f.read() == "content1"
    assert os.path.isfile(setup_bisync["device2"] / "file2.txt")
    with open(setup_bisync["device2"] / "file2.txt") as f:
        assert f.read() == "content2"
    assert os.path.isfile(setup_bisync["device2"] / "folder1" / "file3.txt")
    with open(setup_bisync["device2"] / "folder1" / "file3.txt") as f:
        assert f.read() == "content3"

    # assert remote1
    p = Popen(
        f"rclone ls {setup_bisync['remote1']} {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "file1.txt" in stdout.decode()
    assert "file2.txt" in stdout.decode()
    assert "folder1/file3.txt" in stdout.decode()

    p = Popen(
        f"rclone cat {setup_bisync['remote1']}file1.txt {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "content1" == stdout.decode()
    p = Popen(
        f"rclone cat {setup_bisync['remote1']}file2.txt {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "content2" == stdout.decode()
    p = Popen(
        f"rclone cat {setup_bisync['remote1']}folder1/file3.txt {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "content3" == stdout.decode()
