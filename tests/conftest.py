import configparser
import os
import shutil
import time
from pathlib import Path
from subprocess import PIPE, Popen

import pytest

from src.rClone import create_folder, delete_folder


@pytest.fixture
def config():
    config_str = """
    [DEFAULT]
    testpath = testing_environment/
    bisync = bisync/
    device1 = device1/
    device2 = device2/
    device3 = device3/
    remote1 = remote1:
    remote2 = remote2:
    """
    config = configparser.ConfigParser()
    config.read_string(config_str)
    return config


@pytest.fixture
def setup(config):

    basepath = config["DEFAULT"]["testpath"]
    device1 = config["DEFAULT"]["device1"]
    device2 = config["DEFAULT"]["device2"]
    device3 = config["DEFAULT"]["device3"]
    remote1 = config["DEFAULT"]["remote1"]
    remote2 = config["DEFAULT"]["remote2"]
    bisync = config["DEFAULT"]["bisync"]
    create_folder(folder=basepath)
    create_folder(path=basepath, folder=device1)
    create_folder(path=basepath, folder=device2)
    create_folder(path=basepath, folder=device3)
    create_folder(path=basepath, folder=bisync)
    config_flag = "--config rclone.conf"
    bisync_flag = f"--workdir {Path(basepath)/bisync}"
    yield {
        "device1": Path(basepath) / device1,
        "device2": Path(basepath) / device2,
        "device3": Path(basepath) / device3,
        "bisync": Path(basepath) / bisync,
        "remote1": remote1,
        "remote2": remote2,
        "config_flag": config_flag,
        "bisync_flag": bisync_flag,
    }
    delete_folder(folder=basepath)
    p = Popen(f"rclone delete {remote1} {config_flag} --rmdirs")
    p.wait()
    p = Popen(f"rclone delete {remote2} {config_flag} --rmdirs")
    p.wait()


@pytest.fixture
def setup_files(setup):
    with open(setup["device1"] / "file1.txt", "w") as f:
        f.write("content1")
    with open(setup["device1"] / "file2.txt", "w") as f:
        f.write("content2")
    create_folder(setup["device1"] / "folder1")
    with open(setup["device1"] / "folder1" / "file3.txt", "w") as f:
        f.write("content3")

    p = Popen(
        f"rclone sync {str(setup['device1'])} {setup['remote1']} {setup['config_flag']}"
    )
    p.wait()

    p = Popen(
        f"rclone sync  {setup['remote1']} {str(setup['device2'])} {setup['config_flag']}"
    )
    p.wait()

    return setup


@pytest.fixture
def setup_bisync(setup):

    with open(setup["device1"] / "file1.txt", "w") as f:
        f.write("content1")
    with open(setup["device1"] / "file2.txt", "w") as f:
        f.write("content2")
    create_folder(setup["device1"] / "folder1")
    with open(setup["device1"] / "folder1" / "file3.txt", "w") as f:
        f.write("content3")

    p = Popen(
        f"rclone bisync {str(setup['device1'])} {setup['remote1']} {setup['config_flag']} {setup['bisync_flag']} --resync"
    )
    p.wait()
    p = Popen(
        f"rclone bisync {str(setup['device2'])} {setup['remote1']}  {setup['config_flag']} {setup['bisync_flag']} --resync"
    )
    p.wait()

    return setup


@pytest.fixture
def setup_test():

    yield

    shutil.rmtree("device1")
    shutil.rmtree("device2")
    shutil.rmtree("device3")
    shutil.rmtree("bisync1")
    shutil.rmtree("bisync2")
    p = Popen(f"rclone delete remote1: --config rclone.conf --rmdirs")
    p.wait()
    p = Popen(f"rclone delete remote2: --config rclone.conf --rmdirs")
    p.wait()
