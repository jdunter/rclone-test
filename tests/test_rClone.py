import json
import os
import pathlib
import time
from subprocess import PIPE, Popen

from src.rClone import create_folder, delete_folder


def test_push(setup):
    """
    GIVEN a local file
    WHEN rclone sync is used
    THEN the file should be present on the server and have the same content
    """
    # GIVEN
    name = "testfile.txt"
    content = "lorem ipsum"
    device = setup["device1"]
    remote = setup["remote1"]
    config_flag = setup["config_flag"]
    with open(device / name, "w") as f:
        f.write(content)

    # WHEN
    p = Popen(f"rclone sync {str(device)} {remote} {config_flag}")
    p.wait()

    # THEN
    p = Popen(f"rclone ls {remote} {config_flag}", stdout=PIPE)
    stdout, _ = p.communicate()
    assert name in stdout.decode()

    p = Popen(f"rclone cat {remote}{name} {config_flag}", stdout=PIPE)
    stdout, _ = p.communicate()
    assert content == stdout.decode()


def test_pull(setup):
    """
    GIVEN a remote file and no local files
    WHEN rclone sync is used
    THEN the file should be present locally, file content should be the same locally
        file should be present remotely and file content should be the same remotely
    """
    # GIVEN
    name = "testfile.txt"
    content = "lorem ipsum"
    device = setup["device1"]
    remote = setup["remote1"]
    config_flag = setup["config_flag"]
    with open(device / name, "w") as f:
        f.write(content)

    p = Popen(f"rclone sync {str(device)} {remote} {config_flag}")
    p.wait()
    os.remove(device / name)

    # WHEN
    p = Popen(f"rclone sync {remote} {str(device)} {config_flag}")
    p.wait()

    # THEN
    assert os.path.isfile(device / name)
    with open(device / name) as f:
        content_file = f.read()
        assert content == content_file


def test_bisync_newfile(setup_bisync):
    """
    GIVEN A new file is created on device1
    WHEN bisync happens device1 <> remote1 <> device2
    THEN the new file should also exist on device2 and remotely
        and should contain the content on device2 and remotely
    """
    # GIVEN
    with open(setup_bisync["device1"] / "folder1" / "file4.txt", "w") as f:
        f.write("newContent")

    # WHEN
    p = Popen(
        f"rclone bisync {str(setup_bisync['device1'])} {setup_bisync['remote1']} {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()
    p = Popen(
        f"rclone bisync {str(setup_bisync['device2'])} {setup_bisync['remote1']}  {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )  # NOTE: Writing the remote first and then the device didn't work out
    p.wait()
    # THEN
    # assert device 2
    with open(setup_bisync["device2"] / "folder1" / "file4.txt") as f:
        assert f.read() == "newContent"

    # assert remote
    p = Popen(
        f"rclone ls {setup_bisync['remote1']} {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "folder1/file4.txt" in stdout.decode()
    p = Popen(
        f"rclone cat {setup_bisync['remote1']}folder1/file4.txt {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "newContent" == stdout.decode()


def test_bisync_delete_file(setup_bisync):
    """
    GIVEN A file is deleted on device1
    WHEN bisync happens device1 <> remote1 <> device2
    THEN the new file should also be deleted on device2 and remotely
    """
    # GIVEN
    os.remove(setup_bisync["device1"] / "folder1" / "file3.txt")

    # WHEN
    p = Popen(
        f"rclone bisync {str(setup_bisync['device1'])} {setup_bisync['remote1']} {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()
    p = Popen(
        f"rclone bisync {str(setup_bisync['device2'])} {setup_bisync['remote1']}  {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()

    # THEN
    # assert device 2
    assert not os.path.exists(setup_bisync["device2"] / "folder1" / "file3.txt")

    # assert remote
    p = Popen(
        f"rclone ls {setup_bisync['remote1']} {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "folder1/file3.txt" not in stdout.decode()


def test_bisync_change_file(setup_bisync):
    """
    GIVEN A file is changed on device1
    WHEN bisync happens device1 <> remote1 <> device2
    THEN the new file should also be changed on device2 and remotely
    """
    # Check if file3 already exists and has original content
    # assert device 2

    with open(setup_bisync["device2"] / "folder1" / "file3.txt") as f:
        assert f.read() == "content3"
    # assert remote
    p = Popen(
        f"rclone ls {setup_bisync['remote1']} {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "folder1/file3.txt" in stdout.decode()
    p = Popen(
        f"rclone cat {setup_bisync['remote1']}folder1/file3.txt {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "content3" == stdout.decode()

    # GIVEN
    with open(setup_bisync["device1"] / "folder1" / "file3.txt", "w") as f:
        f.write("newContent")

    # WHEN
    p = Popen(
        f"rclone bisync {str(setup_bisync['device1'])} {setup_bisync['remote1']} {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()
    p = Popen(
        f"rclone bisync {str(setup_bisync['device2'])} {setup_bisync['remote1']}  {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()

    # THEN
    # assert device 2
    with open(setup_bisync["device2"] / "folder1" / "file3.txt") as f:
        assert f.read() == "newContent"

    # assert remote
    p = Popen(
        f"rclone ls {setup_bisync['remote1']} {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "folder1/file3.txt" in stdout.decode()
    p = Popen(
        f"rclone cat {setup_bisync['remote1']}folder1/file3.txt {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "newContent" == stdout.decode()


def test_move_file(setup_bisync):
    """
    GIVEN A file is moved on device 1
    WHEN Bisync is applied on device1 and then device2
    THEN the file should also be moved on device2
    """

    # GIVEN
    source_path = setup_bisync["device1"] / "file1.txt"
    destination_path = setup_bisync["device1"] / "folder1" / "file1.txt"
    source_path.rename(destination_path)

    # WHEN
    p = Popen(
        f"rclone bisync {str(setup_bisync['device1'])} {setup_bisync['remote1']} {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()
    p = Popen(
        f"rclone bisync {str(setup_bisync['device2'])} {setup_bisync['remote1']}  {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()

    # THEN
    assert not os.path.exists(setup_bisync["device2"] / "file1.txt")
    assert os.path.exists(setup_bisync["device2"] / "folder1" / "file1.txt")


def test_sync_new_delete_change(setup_bisync):
    """
    GIVEN A file is changed, a file is deleted and a file is created on device1
    WHEN bisync happens device1 <> remote1 <> device2
    THEN All three changes should be visible on remote and device2
    """

    # GIVEN
    # change
    with open(setup_bisync["device1"] / "folder1" / "file3.txt", "w") as f:
        f.write("newContent3")
    # remove
    os.remove(setup_bisync["device1"] / "file1.txt")
    # create
    with open(setup_bisync["device1"] / "newfile.txt", "w") as f:
        f.write("newContent")

    # WHEN
    p = Popen(
        f"rclone bisync {str(setup_bisync['device1'])} {setup_bisync['remote1']} {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    stdout, _ = p.communicate()
    p = Popen(
        f"rclone bisync {str(setup_bisync['device2'])} {setup_bisync['remote1']}  {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    stdout, _ = p.communicate()

    # THEN
    # assert device 2
    # change
    with open(setup_bisync["device2"] / "folder1" / "file3.txt") as f:
        assert f.read() == "newContent3"
    # remove
    assert not os.path.exists(setup_bisync["device2"] / "file1.txt")
    # new
    with open(setup_bisync["device2"] / "newfile.txt") as f:
        assert f.read() == "newContent"

    # assert remote
    # change
    p = Popen(
        f"rclone ls {setup_bisync['remote1']} {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "folder1/file3.txt" in stdout.decode()
    p = Popen(
        f"rclone cat {setup_bisync['remote1']}folder1/file3.txt {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "newContent3" == stdout.decode()
    # remove
    p = Popen(
        f"rclone ls {setup_bisync['remote1']} {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "file1.txt" not in stdout.decode()
    # new
    p = Popen(
        f"rclone ls {setup_bisync['remote1']} {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "newfile.txt" in stdout.decode()
    p = Popen(
        f"rclone cat {setup_bisync['remote1']}newfile.txt {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "newContent" == stdout.decode()


def test_bisync_newfile_on_each_device(setup_bisync):
    """
    GIVEN A new file is created on device1 and another one on device2
    WHEN bisync happens device1 <> remote1 <> device2
    THEN the new file of device1 should exist on device2. the file of device2 should
        exist remotely, but not on device1. only after a second sync between
            remote and device 1.
    """
    # GIVEN
    with open(setup_bisync["device1"] / "folder1" / "file_device1.txt", "w") as f:
        f.write("newContent_device1")
    with open(setup_bisync["device2"] / "folder1" / "file_device2.txt", "w") as f:
        f.write("newContent_device2")

    # WHEN
    p = Popen(
        f"rclone bisync {str(setup_bisync['device1'])} {setup_bisync['remote1']} {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()

    p = Popen(
        f"rclone bisync {str(setup_bisync['device2'])} {setup_bisync['remote1']}  {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()  # NOTE: Writing the remote first and then the device didn't work out

    # THEN
    # assert device 2
    with open(setup_bisync["device2"] / "folder1" / "file_device1.txt") as f:
        assert f.read() == "newContent_device1"
    # assert file is not on device 1
    assert not os.path.exists(setup_bisync["device1"] / "folder1" / "file_device2.txt")
    # assert remote
    p = Popen(
        f"rclone ls {setup_bisync['remote1']} {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "folder1/file_device1.txt" in stdout.decode()
    assert "folder1/file_device2.txt" in stdout.decode()
    p = Popen(
        f"rclone cat {setup_bisync['remote1']}folder1/file_device1.txt {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "newContent_device1" == stdout.decode()
    p = Popen(
        f"rclone cat {setup_bisync['remote1']}folder1/file_device2.txt {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "newContent_device2" == stdout.decode()

    # Finally sync with device1 again and check if the device2 file is there
    # sync
    p = Popen(
        f"rclone bisync {str(setup_bisync['device1'])} {setup_bisync['remote1']} {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()
    # check
    assert os.path.exists(setup_bisync["device1"] / "folder1" / "file_device2.txt")
    with open(setup_bisync["device1"] / "folder1" / "file_device2.txt") as f:
        assert f.read() == "newContent_device2"


def test_change_same_file_on_two_devices(setup_bisync):
    """
    GIVEN a file is changed on device1 and also on device2
    WHEN a bisync is triggered first on device1 and then on device2
    THEN it should create conflict files remotely and on the second device

    GIVEN the conflict created before
    WHEN one of the conflict file is deleted and the other one renamed
    THEN the conflict should be resolved
    """

    with open(setup_bisync["device1"] / "file1.txt", "w") as f:
        f.write("device1")
    with open(setup_bisync["device2"] / "file1.txt", "w") as f:
        f.write("device2")

    # sync device1 to remote
    p = Popen(
        f"rclone bisync {str(setup_bisync['device1'])} {setup_bisync['remote1']} {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()

    # check if properly synced
    p = Popen(
        f"rclone cat {setup_bisync['remote1']}file1.txt {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "device1" == stdout.decode()

    # try to sync device2 to remote
    p = Popen(
        f"rclone bisync {str(setup_bisync['device2'])} {setup_bisync['remote1']}  {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()

    # check if conflict happened remotely
    p = Popen(
        f"rclone ls {setup_bisync['remote1']} {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "file1.txt.conflict1" in stdout.decode()
    assert "file1.txt.conflict2" in stdout.decode()

    p = Popen(
        f"rclone cat {setup_bisync['remote1']}file1.txt.conflict1 {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "device2" == stdout.decode()
    p = Popen(
        f"rclone cat {setup_bisync['remote1']}file1.txt.conflict2 {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "device1" == stdout.decode()

    # check if conflict files are on device2
    with open(setup_bisync["device2"] / "file1.txt.conflict1") as f:
        assert f.read() == "device2"

    with open(setup_bisync["device2"] / "file1.txt.conflict2") as f:
        assert f.read() == "device1"

    # sync the conflict back to device 1 to prevent misbehaviour of not noticing the
    # conflict resolve
    p = Popen(
        f"rclone bisync {str(setup_bisync['device1'])} {setup_bisync['remote1']} {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()

    # Resolve conflict
    (setup_bisync["device2"] / "file1.txt.conflict2").unlink()
    (setup_bisync["device2"] / "file1.txt.conflict1").rename(
        setup_bisync["device2"] / "file1.txt"
    )
    # this is not necessary if I sync the conflict back to device one:
    # (setup_bisync["device2"] / "file1.txt").touch() #because appearently otherwise rclone doesn't notice, that there was a change

    # Sync again from device2 to remote and then to device 1
    p = Popen(
        f"rclone bisync {str(setup_bisync['device2'])} {setup_bisync['remote1']}  {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()

    p = Popen(
        f"rclone bisync {str(setup_bisync['device1'])} {setup_bisync['remote1']} {setup_bisync['config_flag']} {setup_bisync['bisync_flag']}"
    )
    p.wait()

    # Check if the file contains the same content everywhere
    p = Popen(
        f"rclone cat {setup_bisync['remote1']}file1.txt {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "device2" == stdout.decode()

    with open(setup_bisync["device1"] / "file1.txt") as f:
        assert f.read() == "device2"
    with open(setup_bisync["device2"] / "file1.txt") as f:
        assert f.read() == "device2"


def test_ignore(setup_bisync):
    """
    GIVEN a file is on device1 and is in the ignore file
    WHEN the file is synced with remote1
    THEN the file should not appear in remote1

    GIVEN a file is on remote1 and ignored by device2
    WHEN device2 syncs with remote1
    THEN the file shouldn't appear in device2
    """

    # create filter file
    with open(setup_bisync["bisync"] / "filter_device1.txt", "w") as f:
        f.write(
            """# Filter File device1
        - /not_remote/**
        """
        )
    with open(setup_bisync["bisync"] / "filter_device2.txt", "w") as f:
        f.write(
            """# Filter File device2
        - /not_device2/**
        """
        )

    # create files to filter
    create_folder(folder="not_remote", path=setup_bisync["device1"])
    create_folder(folder="not_device2", path=setup_bisync["device1"])
    create_folder(folder="for_device2", path=setup_bisync["device1"])
    with open(setup_bisync["device1"] / "not_remote" / "secret1.txt", "w") as f:
        f.write("secret1")
    with open(setup_bisync["device1"] / "not_device2" / "secret2.txt", "w") as f:
        f.write("secret2")
    with open(setup_bisync["device1"] / "for_device2" / "not_a_secret.txt", "w") as f:
        f.write("not_a_secret")

    # sync device1 to remote
    p = Popen(
        f"rclone bisync {str(setup_bisync['device1'])} "
        + f"{setup_bisync['remote1']} {setup_bisync['config_flag']} "
        + f"{setup_bisync['bisync_flag']} --filter-from {setup_bisync['bisync'] / 'filter_device1.txt'}"
    )
    p.wait()

    # check if not_remote is not on remote but not_device2 should be there
    p = Popen(
        f"rclone ls {setup_bisync['remote1']} {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "not_remote" not in stdout.decode()
    assert "not_device2" in stdout.decode()
    assert "for_device2" in stdout.decode()

    # sync device2 to remote
    p = Popen(
        f"rclone bisync {str(setup_bisync['device2'])} "
        + f"{setup_bisync['remote1']} {setup_bisync['config_flag']} "
        + f"{setup_bisync['bisync_flag']} --filter-from {setup_bisync['bisync'] / 'filter_device2.txt'}"
    )
    p.wait()

    # device 2 should have only for_device2 and not not_remote or not_device2
    assert pathlib.Path.is_dir(setup_bisync["device2"] / "for_device2")
    assert not pathlib.Path.is_dir(setup_bisync["device2"] / "not_remote")
    assert not pathlib.Path.is_dir(setup_bisync["device2"] / "not_device2")


def test_get_size(setup_bisync):
    """
    HAVING a remote
    WHEN running "rclone size"
    THEN it should show the size
    """
    p = Popen(
        f"rclone size {setup_bisync['remote1']} {setup_bisync['config_flag']}",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    assert "Total size:" in stdout.decode()


def test_get_size_filter(setup_bisync):
    """
    HAVING a remote
    WHEN running "rclone size" with a filter
    THEN the size should be smaller than without filter
    """
    p = Popen(
        f"rclone size {setup_bisync['remote1']} {setup_bisync['config_flag']} --json",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    output1 = json.loads(stdout.decode())

    # create filter file
    with open(setup_bisync["bisync"] / "filter.txt", "w") as f:
        f.write(
            """# Filter File
        - /folder1/**
        """
        )
    # get size with filter
    p = Popen(
        f"rclone size {setup_bisync['remote1']} "
        + f"{setup_bisync['config_flag']} "
        + f"--filter-from {setup_bisync['bisync'] / 'filter.txt'}"
        + " --json",
        stdout=PIPE,
    )
    stdout, _ = p.communicate()
    output2 = json.loads(stdout.decode())
    assert output1["bytes"] > output2["bytes"]
    assert output1["count"] > output2["count"]
