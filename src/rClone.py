import os
import shutil
from pathlib import Path


def create_folder(folder="", path=""):
    final_path = Path(path) / folder
    if os.path.exists(final_path):
        Exception("Path already exists")
    os.makedirs(final_path)


def delete_folder(folder="", path=""):
    final_path = Path(path) / folder
    if not os.path.exists(final_path):
        Exception("Path doesn't exist")
    shutil.rmtree(final_path)


def create_file(name, content="", path=""):
    path = Path(path)
    with open(path / name, "w") as f:
        f.write(content)
